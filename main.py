from agents.VietOCR import VietOCRAgent
from utils.helper import load_config

def main():
    config = load_config()
    agent = VietOCRAgent(config)
    agent.train()
    

if __name__ == '__main__':
    main()
    