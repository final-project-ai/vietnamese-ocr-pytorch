import torch
import torch.nn as nn 
from graphs.models.backbone import vgg
from torchsummary import summary


class CNN(nn.Module):
    def __init__(self, backbone, **kwargs):
        super(CNN, self).__init__()

        if backbone == "vgg11_bn":
            self.model = vgg.vgg11_bn(**kwargs)
        else:
            self.model = vgg.vgg19_bn(**kwargs)

    def forward(self, x):
        out = self.model(x)
        return out 
    
    def freeze(self):
        for name, param in self.model.features.named_parameters():
            if name != 'last_conv_1x1':
                param.requires_grad = False

    def unfreeze(self):
        for param in self.model.features.parameters():
            param.requires_grad = True

def main():
    import sys 
    sys.path.append(".")
    sys.path.append("../../")
    from utils.helper import load_json

    config = load_json("configs/vgg-transformer.json")
    backbone = config["backbone"]
    model = CNN(backbone, **config["cnn"])
    summary(model, (3, 224, 224))


if __name__ == "__main__":
    main()