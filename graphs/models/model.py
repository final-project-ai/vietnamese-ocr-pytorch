import torch 
import torch.nn as nn
from torchsummary import summary
from graphs.models.cnn import CNN
from graphs.models.transformer import LanguageTransformer


class VietOCR(nn.Module):
    def __init__(self, vocab_size,
                backbone,
                cnn_args,
                transformer_args):

        super(VietOCR, self).__init__()

        self.cnn = CNN(backbone, **cnn_args)
        self.transformer = LanguageTransformer(vocab_size, **transformer_args)


    def forward(self, img, tgt_input=torch.randint(0, 2, (64, 2)), 
                    tgt_key_padding_mask=torch.randint(0, 2, (2, 64))):
        """
        Shape:
            - img: (N, C, H, W)
            - tgt_input: (T, N)
            - tgt_key_padding_mask: (N, T)
            - output: b t v
        """

        # if you want to summary model architechture, you must uncomment below

        encode = self.cnn(img)
        out = self.transformer(encode, tgt_input, tgt_key_padding_mask=tgt_key_padding_mask)

        return out


def main():
    import sys 
    sys.path.append(".")
    sys.path.append("../../")

    from utils.helper import load_json
    config = load_json("configs/vgg-transformer.json")
    config1 = load_json("configs/base.json")
    model = VietOCR(vocab_size=500, backbone="vgg19_bn", cnn_args=config["cnn"], transformer_args=config1["transformer"])
    summary(model, (3, 32, 128))


if __name__ == '__main__':
    main()
    


